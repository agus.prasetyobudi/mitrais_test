const express = require('express'),
path = require('path'),
logger = require('morgan'),
bodyParser = require('body-parser'),
index = require('./controller/main'),
data = require('./controller/flow'),
api = require('./router/api')
const app = express()
const port = 3000


app.set("views", path.join(__dirname, "views"))
app.set("view engine", "ejs")
app.use(logger("dev"))
app.use(bodyParser.json())
app.use(express.urlencoded({extended:true})) 
app.use(express.static(path.join(__dirname, "assets")))
app.get('/',index.data)
app.use('/api', api)
app.listen(port, () => console.log(`Example app listening on port `+port+` !`))