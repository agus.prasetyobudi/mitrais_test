const db = require('../models/index'),
user = db.user

exports.insert = (req,res)=>{
    let body = JSON.parse(JSON.stringify(req.body)),
    push_data = {
        "firstname"     :body.First_name,
        "lastname"      :body.Last_name,
        "email"         :body.email,
        "dateBirth"     :body.DateBirth,
        "gender"        :body.gender,
        "phoneNumber"   :body.phone,
        "password"      :body.password
    }
    user.create(push_data)
    .then((x)=>{
        if(x != null){
            res.status(200).json({
                "Status" : 200,
                "Message": "Data Inserted"
            })
        }else{
            res.status(404).json({
                "Status" : 404,
                "Message": null
            })
        }
    })
}
exports.login = (req, res)=>{
    user.findOne({
    attributes:['id','email','firstname','lastname','password'],
    where:{
        email:req.body.email
    }
})
.then((results)=>{
    console.log(results)
    if(results.dataValues.password == req.body.password){
        res.status(200).json({
            'error': false,
            'message' : "Data Found",
            "data" : [{
                "first_name" : results.dataValues.firstname,
                "last_name" : results.dataValues.lastname
            }]
        })
    }else{
        res.status(400).json({
            'error': true,
            'message' : 'something wrong',
            'data' : null
        })
    }
})
 
}