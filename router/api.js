const express = require('express'),
Routes = express.Router(),
flow = require('../controller/flow')

Routes.post('/insert', flow.insert)
Routes.post('/login', flow.login)

module.exports = Routes